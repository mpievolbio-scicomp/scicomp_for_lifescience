{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to parallel computing with jupyter and ipyparallel\n",
    "Heavily reduced from https://computing.llnl.gov/tutorials/parallel_comp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parallel computing taxonomy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are four types of parallel computing (known as Flynn's classical taxonomy)\n",
    "\n",
    "![Flynn's classical taxonomy](https://computing.llnl.gov/tutorials/parallel_comp/images/flynnsTaxonomy.gif)  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Examples:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### SISD (Single Instruction Single Data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "![SISD](https://computing.llnl.gov/tutorials/parallel_comp/images/sisd2.gif)\n",
    "##### Genomics\n",
    "* Input data: A fasta file with 1 sequence\n",
    "* Instruction: Align the sequence to a reference sequence\n",
    "\n",
    "##### Numerics\n",
    "* Input data: A float x=3.14\n",
    "* Compute the square of x\n",
    "\n",
    " "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### SIMD"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "![SIMD](https://computing.llnl.gov/tutorials/parallel_comp/images/simd3.gif)\n",
    "##### Genomics\n",
    "* Input data: A fasta file with 1000 sequences\n",
    "* Instruction: Align each sequence to a reference sequence, process batches of N sequences in parallel.\n",
    "\n",
    "##### Numerics\n",
    "* Input data: An array: [1,2,3,4,5]\n",
    "* Instruction: Square all elements of the array in parallel"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### MISD"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "![MISD](https://computing.llnl.gov/tutorials/parallel_comp/images/misd4.gif)\n",
    "##### Genomics\n",
    "* Input data: A fasta file with 1 sequence\n",
    "* Instructions: Perform [alignment, reversal, transcription, mutation] on the sequence (in parallel)\n",
    "\n",
    "##### Numerics\n",
    "* Input data: A float x=3.14\n",
    "* Instructions: Calculate n'th power of x, n=1,2,3,4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### MIMD"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "![MIMD](https://computing.llnl.gov/tutorials/parallel_comp/images/mimd2.gif)\n",
    "\n",
    "##### Genomics\n",
    "* Input data: fasta file with 1000 sequences\n",
    "* Instructions: [align, reverse, transcribe, mutate] each sequence\n",
    "\n",
    "##### Numerics:\n",
    "* Input data: Array of floats [x0, x1, x2, ...]\n",
    "* Instructions: Compute n'th power of each element, n=1,2,3,4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Memory architectures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Shared memory"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "#### UMA (Uniform Memory Access)\n",
    "![UMA](https://computing.llnl.gov/tutorials/parallel_comp/images/shared_mem.gif)\n",
    "\n",
    "#### NUMA (Non-Uniform Memory Access)\n",
    "![NUMA](https://computing.llnl.gov/tutorials/parallel_comp/images/numa.gif)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Distributed memory"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "![Distributed Memory](https://computing.llnl.gov/tutorials/parallel_comp/images/distributed_mem.gif)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Hybrid architecture"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "![Hybrid](https://computing.llnl.gov/tutorials/parallel_comp/images/hybrid_mem2.gif)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Shared memory parallelism (Today's topic)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### General Characteristics:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "* All processors to access all memory as global address space.\n",
    "* Multiple processors can operate independently but share the same memory resources.\n",
    "* Changes in a memory location effected by one processor are visible to all other processors.\n",
    "* Advantages:\n",
    "    - Global address space provides a user-friendly programming perspective to memory\n",
    "    - Data sharing between tasks is both fast and uniform due to the proximity of memory to CPUs \n",
    "* Disadvantages:\n",
    "    - Lack of scalability between memory and CPUs.\n",
    "    - Programmer responsibility for synchronization constructs that ensure \"correct\" access of global memory. \n",
    "   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Python support: multiprocessing module"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "`multiprocessing` is the default python package to support shared memory task parallelism. However, `multiprocess` does not work if used in a jupyter notebook environment such as this one. Therefore, here, we use the `multiprocess` module (a fork off `multiprocessing`). See [this stackoverflow post](https://stackoverflow.com/questions/48846085/python-multiprocessing-within-jupyter-notebook) if you are interested in the technical background. To install `multiprocess` run (in a terminal)\n",
    "```\n",
    "pip install multiprocess\n",
    "```\n",
    "if you are in a pip environment, **OR**\n",
    "```\n",
    "conda install multiprocess -c conda-forge\n",
    "```\n",
    "if you work in a conda environment.\n",
    "\n",
    "An alternative solution is the `ipyparallel` package. `ipyparallel` also supports distributed memory parallelism (with MPI) and will be showcased further below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "import multiprocess"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A SIMD example with `multiprocess`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "#### Instruction\n",
    "* Square all numbers in an array"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([   0,    1,    2, ..., 9997, 9998, 9999])"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x = numpy.arange(10000); x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Using the `multiprocess` programming interface"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "* Inspect our local compute environment\n",
    "* Setup a pool of *workers*\n",
    "* Define a function that implements the instruction (calculate the square of the input)\n",
    "* Instruct the pool to perform the instructions on the input data\n",
    "* Join the returns from each *worker*.\n",
    "* Inspect the results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Inspect our resources"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "64"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "number_of_cpus = multiprocess.cpu_count(); number_of_cpus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### The pool"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "We have far more tasks (10000) than CPUs. Therefore, we will utilize the maximum number of CPUs available on our system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\u001b[0;31mSignature:\u001b[0m\n",
       "\u001b[0mmultiprocess\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mPool\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\u001b[0m\n",
       "\u001b[0;34m\u001b[0m    \u001b[0mprocesses\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;32mNone\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\n",
       "\u001b[0;34m\u001b[0m    \u001b[0minitializer\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;32mNone\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\n",
       "\u001b[0;34m\u001b[0m    \u001b[0minitargs\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\n",
       "\u001b[0;34m\u001b[0m    \u001b[0mmaxtasksperchild\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;32mNone\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\n",
       "\u001b[0;34m\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
       "\u001b[0;31mDocstring:\u001b[0m Returns a process pool object\n",
       "\u001b[0;31mFile:\u001b[0m      ~/.conda/envs/default/lib/python3.7/site-packages/multiprocess/context.py\n",
       "\u001b[0;31mType:\u001b[0m      method\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "pool = multiprocess.Pool?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "pool = multiprocess.Pool(processes=number_of_cpus)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### The instruction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "def square(x):\n",
    "    return x**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Test the instruction\n",
    "assert square(2.0) == 4.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Let the workers do their job."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\u001b[0;31mSignature:\u001b[0m \u001b[0mpool\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mmap\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mfunc\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0miterable\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mchunksize\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;32mNone\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
       "\u001b[0;31mDocstring:\u001b[0m\n",
       "Apply `func` to each element in `iterable`, collecting the results\n",
       "in a list that is returned.\n",
       "\u001b[0;31mFile:\u001b[0m      ~/.conda/envs/default/lib/python3.7/site-packages/multiprocess/pool.py\n",
       "\u001b[0;31mType:\u001b[0m      method\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "results = pool.map?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = pool.map(square, x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Look at the results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([       0,        1,        4, ..., 99940009, 99960004, 99980001])"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "numpy.array(results)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check for correctness\n",
    "assert numpy.linalg.norm(numpy.array(results) - x**2) == 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Distributed memory parallelism"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Message passing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References and further resources"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Web resources\n",
    "* LLNL parallel computing: https://computing.llnl.gov/tutorials/parallel_comp/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Default custom kernel",
   "language": "python",
   "name": "default"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
